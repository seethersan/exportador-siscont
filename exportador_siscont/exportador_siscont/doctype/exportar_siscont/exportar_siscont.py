# -*- coding: utf-8 -*-
# Copyright (c) 2018, OVENUBE and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

from exportador_siscont.exportador_siscont.utils import Utils, to_file


class ExportarSiscont(Utils):
	def get_account(self, year, periodo):
		account_list = []
		from_date, to_date = self.get_dates(year, periodo)

		account = frappe.db.sql("""select
						IF(voucher_type = 'Purchase Invoice', '02', IF(voucher_type = 'Sales Invoice', '01', '03')) as origen,
						(SELECT
							COUNT(name)
						FROM 
							`tabGL Entry` as gl_1
						WHERE SUBSTRING(gl_1.name,3) <= SUBSTRING(gl.name,3)) nro_voucher,
						DATE_FORMAT(gl.posting_date,'%d/%m/%Y') as fecha,
						SUBSTRING(gl.account,1,POSITION('-' in gl.account)-2) as codigo_asiento,
						IF(gl.debit_in_account_currency = 0, '0.00', ROUND(gl.debit_in_account_currency, 2)) as debe,
						IF(gl.credit_in_account_currency = 0, '0.00', ROUND(gl.credit_in_account_currency, 2)) as haber,
						IF(gl.account_currency = 'SOL' OR 'PEN', 'S', 'D') as moneda,
						IFNULL(IF(voucher_type = 'Purchase Invoice',
												(select 
													conversion_rate
												from
													`tabPurchase Invoice`where name=voucher_no),
												(select 
													conversion_rate
												from 
													`tabSales Invoice`
												where name=voucher_no)),'1.000') as tipo_cambio,
						IF(voucher_type = 'Purchase Invoice',
												(select 
													IF(LENGTH(codigo_comprobante) = 1, CONCAT('0', codigo_comprobante), codigo_comprobante)
												from
													`tabPurchase Invoice`where name=voucher_no),
						IF(voucher_type = 'Sales Invoice',
												(select 
													IF(LENGTH(codigo_comprobante) = 1, CONCAT('0', codigo_comprobante), codigo_comprobante) 
												from 
													`tabSales Invoice`
												where name=voucher_no),
						IF(voucher_type = 'Delivery Note', (select 
													IF(LENGTH(codigo_tipo_comprobante) = 1, CONCAT('0', codigo_tipo_comprobante), codigo_tipo_comprobante) 
												from 
													`tabDelivery Note`
												where name=voucher_no),
						IF(voucher_type = 'Purchase Receipt', (select 
													IF(LENGTH(codigo_tipo_comprobante) = 1, CONCAT('0', codigo_tipo_comprobante), codigo_tipo_comprobante) 
												from 
													`tabPurchase Receipt`
												where name=voucher_no),'')))) as codigo_comprobante,
						CONCAT(IF(voucher_type = 'Purchase Invoice',IFNULL(
												(select 
													bill_series 
												from 
													`tabPurchase Invoice`
												where name=voucher_no),''),
												SUBSTRING_INDEX(SUBSTRING_INDEX(voucher_no,'-',-2),'-',1)),'-',
						IF(voucher_type = 'Purchase Invoice',
												(select 
													bill_no
												from 
													`tabPurchase Invoice`
												where name=voucher_no), SUBSTRING_INDEX(SUBSTRING_INDEX(voucher_no,'-',-2),'-',-1))) as numero_comprobante,
						DATE_FORMAT(gl.posting_date,'%d/%m/%Y') as fechad,
						IFNULL(IF(voucher_type = 'Purchase Invoice',
												(select 
													DATE_FORMAT(due_date,'%d/%m/%Y')
												from
													`tabPurchase Invoice`where name=voucher_no),
												(select 
													DATE_FORMAT(due_date,'%d/%m/%Y')
												from 
													`tabSales Invoice`
												where name=voucher_no)),DATE_FORMAT(gl.posting_date,'%d/%m/%Y')) as fechav,
						IF(voucher_type = 'Purchase Invoice',
												(select 
													`tax_id`
												from
													`tabPurchase Invoice`where name=voucher_no),
												IF(voucher_type = 'Sales Invoice', 
												(select 
													`tax_id` 
												from 
													`tabSales Invoice`
												where name=voucher_no),
												IF(voucher_type = 'Payment Entry',
												(select
													tax_id
												FROM
													`tabPayment Entry` as pe, `tabCustomer` as cu
												WHERE
													cu.name = pe.title
													AND pe.name = gl.voucher_no),''))) as tax_id,
						IFNULL(gl.cost_center,'') as centro_costo,
						'' as codigo_financiero,
						'' as presupuesto,
						IF(voucher_type = 'Payment Entry', 
							(SELECT
								IF(mode_of_payment = "Cheque", '007',
									IF(mode_of_payment = "Efectivo", '008',
										IF(mode_of_payment = "Transferencia bancaria", '003',
											IF(mode_of_payment = "Giro Bancario", '002',''))))
							FROM 
								`tabPayment Entry` as pe
							WHERE pe.name = gl.voucher_no), '') as tipo_pago,
						gl.remarks as glosa,
						IF(SUBSTRING(gl.voucher_no,0,2)='ND' OR 'NC',voucher_no,'') as ref_nota,
						IF(SUBSTRING(gl.voucher_no,0,2)='ND','08',IF(SUBSTRING(gl.voucher_no,0,2)='NC','07','')) as tipo_nota,
						IF(SUBSTRING(gl.voucher_no,0,2)='ND' OR 'NC',DATE_FORMAT(gl.posting_date,'%d/%m/%Y'),'') as fecha_nota,
						'' as numero_detrac,
						'' as fecha_detrac,
						IF(voucher_type='Purchase Invoice','C',IF(voucher_type='Sales Invoice','V','')) as tl,
						IF(voucher_type='Purchase Invoice' AND account LIKE '42%', 
							ROUND((select
								IF(base_total<0,(base_total*-1),base_total)
							FROM 
								`tabPurchase Invoice` as pi
							WHERE pi.name=gl.voucher_no), 2), 
							IF(voucher_type='Sales Invoice' AND account LIKE '1%', ROUND((select
								IF(base_total<0,(base_total*-1),base_total)
							FROM 
								`tabSales Invoice` as si
							WHERE si.name=gl.voucher_no), 2), '')) as subtotal,
						0.00 as subtotal_no_grv,
						IF(voucher_type='Purchase Invoice' AND account LIKE '42%', ROUND(IF(gl.debit_in_account_currency=0,gl.credit_in_account_currency,gl.debit_in_account_currency),2) -
							ROUND((select
								IF(base_total<0,(base_total*-1),base_total)
							FROM 
								`tabPurchase Invoice` as pi
							WHERE pi.name=gl.voucher_no), 2), 
							IF(voucher_type='Sales Invoice' AND account LIKE '1%', ROUND(IF(gl.debit_in_account_currency=0,gl.credit_in_account_currency,gl.debit_in_account_currency),2) -
							ROUND((select
								IF(base_total<0,(base_total*-1),base_total)
							FROM 
								`tabSales Invoice` as si
							WHERE si.name=gl.voucher_no), 2), '')) as igvb,
						0.00 as total_exp,
						0.00 as total_otros_imp,
						IF(voucher_type='Purchase Invoice' AND account LIKE '42%', ROUND(IF(gl.debit_in_account_currency=0,gl.credit_in_account_currency,gl.debit_in_account_currency),2) -
							ROUND((select
								IF(base_total<0,(base_total*-1),base_total)
							FROM 
								`tabPurchase Invoice` as pi
							WHERE pi.name=gl.voucher_no), 2), 
							IF(voucher_type='Sales Invoice' AND account LIKE '1%', ROUND(IF(gl.debit_in_account_currency=0,gl.credit_in_account_currency,gl.debit_in_account_currency),2) -
							ROUND((select
								IF(base_total<0,(base_total*-1),base_total)
							FROM 
								`tabSales Invoice` as si
							WHERE si.name=gl.voucher_no), 2), '')) as igvc,
						0.00 as isc,
						IF(voucher_type = 'Purchase Invoice',
												(select 
													`tax_id`
												from
													`tabPurchase Invoice`where name=voucher_no),
												IF(voucher_type = 'Sales Invoice', 
												(select 
													`tax_id` 
												from 
													`tabSales Invoice`
												where name=voucher_no),
												IF(voucher_type = 'Payment Entry',
												(select
													tax_id
												FROM
													`tabPayment Entry` as pe, `tabCustomer` as cu
												WHERE
													cu.name = pe.title
													AND pe.name = gl.voucher_no),
												IF(voucher_type = 'Delivery Note',
												(select
													tax_id
												FROM
													`tabDelivery Note` as dn
												WHERE
													dn.name = gl.voucher_no),
												IF(voucher_type = 'Purchase Receipt',
												(SELECT
													su.tax_id
												FROM
													`tabPurchase Receipt` as pr, `tabSupplier` as su
												WHERE
													su.name = pr.supplier
													AND pr.name = gl.voucher_no),''))))) as ruc,
						IF(voucher_type = 'Purchase Invoice', '2', IF(voucher_type = 'Sales Invoice', '1', '3')) as tipo,
						IF(voucher_type = 'Purchase Invoice',
												(select 
													supplier_name
												from
													`tabPurchase Invoice`where name=voucher_no),
												IF(voucher_type = 'Sales Invoice', 
												(select 
													customer_name 
												from 
													`tabSales Invoice`
												where name=voucher_no),
												IF(voucher_type = 'Payment Entry',
												(select
													party_name
												FROM
													`tabPayment Entry` as pe, `tabCustomer` as cu
												WHERE
													cu.name = pe.title
													AND pe.name = gl.voucher_no),
												IF(voucher_type = 'Delivery Note',
												(select
													customer_name
												FROM
													`tabDelivery Note` as dn
												WHERE
													dn.name = gl.voucher_no),
												IF(voucher_type = 'Purchase Receipt',
												(SELECT
													supplier_name
												FROM
													`tabPurchase Receipt` as pr
												WHERE
													pr.name = gl.voucher_no),''))))) as razon_social,
						'' as ape_pat,
						'' as ape_mat,
						'' as nombre,
						IF(voucher_type = 'Purchase Invoice',
												(select 
													su.`codigo_tipo_documento`
												from
													`tabPurchase Invoice` as pi,
													`tabSupplier` as su
												where su.name = pi.supplier
												and pi.name=voucher_no),
												IF(voucher_type = 'Sales Invoice', 
												(select 
													cu.`codigo_tipo_documento` 
												from 
													`tabSales Invoice` as si,
													`tabCustomer` as cu
												where cu.name = si.customer
												and si.name=voucher_no),
												IF(voucher_type = 'Payment Entry',
												(select
													cu.`codigo_tipo_documento`
												FROM
													`tabPayment Entry` as pe, `tabCustomer` as cu
												WHERE
													cu.name = pe.title
													AND pe.name = gl.voucher_no),
												IF(voucher_type = 'Delivery Note',
												(select
													cu.codigo_tipo_documento
												FROM
													`tabDelivery Note` as dn, `tabCustomer` as cu
												WHERE
													cu.name = dn.customer
													AND dn.name = gl.voucher_no),
												IF(voucher_type = 'Purchase Receipt',
												(SELECT
													su.codigo_tipo_documento
												FROM
													`tabPurchase Receipt` as pr, `tabSupplier` as su
												WHERE
													su.name = pr.supplier
													AND pr.name = gl.voucher_no),''))))) as tipo_doc,
						'' as rnumdes,
						'' as rcodtasa,
						'' as rindret,
						IF(SUBSTRING(gl.voucher_no,0,2)='NC',
						(SELECT
							sa.net_total
						FROM
							`tabSales Invoice` as sa
						WHERE
							sa.name = gl.voucher_no),'') as monto_nc,
						IF(SUBSTRING(gl.voucher_no,0,2)='NC',
						(SELECT
							sa.net_total
						FROM
							`tabSales Invoice` as sa
						WHERE
							sa.name = gl.voucher_no) * 0.18,'') as igv_nc,
						'' as bien
					from 
						`tabGL Entry` gl
					where SUBSTRING(account,1,POSITION('-' in account)-1) > 100
					and posting_date > '""" + str(from_date) + """' 
					and posting_date < '""" + str(to_date) + """' 
					order by posting_date""", as_dict=True)

		for d in account:
			account_list.append({
				'origen': d.origen,
				'nro_voucher': d.nro_voucher,
				'fecha': d.fecha,
				'codigo_asiento': d.codigo_asiento,
				'debe': d.debe,
				'haber': d.haber,
				'moneda': d.moneda,
				'tipo_cambio': d.tipo_cambio,
				'codigo_comprobante': d.codigo_comprobante,
				'numero_comprobante,': d.numero_comprobante,
				'fechad': d.fechad,
				'fechav': d.fechav,
				'tax_id': d.tax_id,
				'centro_costo': d.centro_costo,
				'codigo_financiero': d.codigo_financiero,
				'presupuesto': d.presupuesto,
				'tipo_pago': d.tipo_pago,
				'ref_nota': d.ref_nota,
				'tipo_nota': d.tipo_nota,
				'fecha_nota': d.fecha_nota,
				'numero_detrac': d.numero_detrac,
				'fecha_detrac': d.fecha_detrac,
				'tl': d.tl,
				'subtotal': d.subtotal,
				'igvb': d.igvb,
				'total_exp': d.total_exp,
				'total_otros_imp': d.total_otros_imp,
				'igvc': d.igvc,
				'isc': d.isc,
				'ruc': d.ruc,
				'tipo': d.tipo,
				'razon_social': d.razon_social,
				'ape_pat': d.ape_pat,
				'ape_mat': d.ape_mat,
				'nombre': d.nombre,
				'tipo_doc': d.tipo_doc,
				'rnumdes': d.rnumdes,
				'rcodtasa': d.rcodtasa,
				'rindret': d.rindret,
				'monto_nc': d.monto_nc,
				'igv_nc': d.igv_nc,
				'bien': d.bien
			})
		return account_list

	def export_siscont(self, year, periodo, ruc):
		data = self.get_account(year, periodo)
		codigo_periodo = self.file_name(year, periodo)
		nombre = "SISCONT" + str(ruc) + codigo_periodo
		nombre = nombre + ".txt"
		return to_file(data, nombre)
