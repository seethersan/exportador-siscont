# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os.path
import distutils.dir_util

def after_install():
    create_dirs()


def create_dirs():
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, "siscont")
    distutils.dir_util.mkpath(path)